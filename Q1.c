#include <stdio.h>

int main(){
    printTriangle(4);
}

void printTriangle(int num){
    for(int i=1;i<=num;i++){
        print(i);
        printf("\n");
    }
}

int print(int num){
    if(num>=1){
        printf("%d ",num);
        print(num-1);
    }
}

