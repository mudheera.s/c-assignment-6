#include <stdio.h>
int main(void)
{
    fibonacciSeq(10);
    return 0;
}

 void fibonacciSeq(int num){
    for(int n = 0; n <= 10; n++){
        printf("%d\n", fibonacci(n));
    }
 }

int fibonacci(int num){
    if(num == 0 || num == 1){
        return num;
    }

    else{
        return fibonacci(num-1) + fibonacci(num-2);
    }
}
